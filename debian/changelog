libnet-twitter-perl (4.01043-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit, Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libio-socket-ssl-perl,
      libnet-http-perl.
    + libnet-twitter-perl: Drop versioned constraint on libio-socket-ssl-perl,
      libnet-http-perl in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:08:06 +0100

libnet-twitter-perl (4.01043-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ gregor herrmann ]
  * Import upstream version 4.01043.
  * Update years of upstream and packaging copyright.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Thu, 18 Jan 2018 18:04:08 +0100

libnet-twitter-perl (4.01042-1) unstable; urgency=medium

  * Team upload

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Florian Schlichting ]
  * Import upstream version 4.01042
  * Drop build-dependency on M::B, upstream switched to EU::MM
  * Add deprecation notice to NEWS and Description
  * Declare compliance with Debian Policy 4.1.1

 -- Florian Schlichting <fsfs@debian.org>  Fri, 24 Nov 2017 13:17:35 +0100

libnet-twitter-perl (4.01041-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Salvatore Bonaccorso ]
  * debian/control: Remove Franck Joncourt from Uploaders.
    Thanks to Tobias Frost (Closes: #831308)

  [ gregor herrmann ]
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Jotam Jr. Trejo from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!
  * Import upstream version 4.01041.
  * Use HTTPS for Twitter URL in debian/copyright. Thanks to duck.

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Nov 2016 19:19:20 +0100

libnet-twitter-perl (4.01020-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Import upstream version 4.01020.
  * Update years of upstream and packaging copyright.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 3.9.8.

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 Apr 2016 15:37:34 +0200

libnet-twitter-perl (4.01010-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata

  * Import upstream version 4.01010
  * Update years of packaging copyright.
  * Add (build) dependency on libio-socket-ssl-perl.
  * Declare compliance with Debian Policy 3.9.6.
  * Drop version from libmodule-build-perl build dependency.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 May 2015 19:54:11 +0200

libnet-twitter-perl (4.01005-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Tue, 12 Aug 2014 21:39:27 +0200

libnet-twitter-perl (4.01004-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 16 May 2014 17:50:32 +0200

libnet-twitter-perl (4.01003-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Axel Beckert ]
  * Remove Antony Gelberg from Uploaders as suggested by him

  [ Daniel Lintott ]
  * Imported Upstream version 4.01003
  * Add myself to uploaders
  * Update upstream copyright years

 -- Daniel Lintott <daniel@serverb.co.uk>  Mon, 07 Apr 2014 16:40:13 +0100

libnet-twitter-perl (4.01002-1) unstable; urgency=medium

  * New upstream release.
    - Warn if ssl option to new is not passed (deprecation cycle)
    Closes: #735366
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 20 Jan 2014 21:18:09 +0100

libnet-twitter-perl (4.01000-1) unstable; urgency=medium

  * New upstream release.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Tue, 10 Dec 2013 19:56:39 +0100

libnet-twitter-perl (4.00007-1) unstable; urgency=low

  * New upstream release.
    Fixes "RateLimit can no longer be used with API::RESTv1_1":
    documentation updated to reality.
    (Closes: #712008)
  * Switch order of versioned (build) dependency on Net::HTTP.
  * Update long description. Point to StatusNet instead of identi.ca
    (which uses pump.io instead of StatusNet by now).

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Oct 2013 17:02:53 +0200

libnet-twitter-perl (4.00006-1) unstable; urgency=low

  * New upstream release.
  * Install "real" manpage from Net/Twitter.pod as Net::Twitter(3pm).
    Thanks to Vincent Lefevre for the bug report. (Closes: #712006)

 -- gregor herrmann <gregoa@debian.org>  Wed, 12 Jun 2013 17:06:07 +0200

libnet-twitter-perl (4.00004-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Alessandro Ghedini ]
  * Remove myself from Uploaders

  [ gregor herrmann ]
  * New upstream release. Fixes "version 4.x required for new Twitter API".
    (Closes: #702486)
  * debian/copyright: update years of upstream and packaging copyright,
    remove info about removed third-party files.
  * debian/NEWS: add a note about migrating to the new Twitter API v1.1.
  * Update build and runtime dependencies.
  * Set Standards-Version to 3.9.4 (no changes).
  * debian/rules: remove hashbang from Twitter.pod.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Mar 2013 13:35:22 +0100

libnet-twitter-perl (3.18003-1) unstable; urgency=low

  * Imported Upstream version 3.18003
  * debian/control:
    - removed versioned dependency for packets that have been superseded
      in all distributions
    - droped libtest-simple-perl from B-D-I, it's part of the core
      since perl >= 5.6.2
  * Upgraded debhelper compatibility level to 9
  * Added myself to Copyright and Uploaders

 -- Jotam Jr. Trejo <jotamjr@debian.org.sv>  Wed, 27 Jun 2012 20:39:31 -0600

libnet-twitter-perl (3.18002-1) unstable; urgency=low

  [ Alessandro Ghedini ]
  * Email change: Alessandro Ghedini -> ghedo@debian.org

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright for inc/Module/* and debian/*.
  * debian/copyright: update to Copyright-Format 1.0.
  * Bump Standards-Version to 3.9.3 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 26 May 2012 20:29:43 +0200

libnet-twitter-perl (3.18001-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Drop patches, both applied upstream.
  * Update years of packaging copyright.
  * Update build and runtime dependencies as per changed upstream
    requirements.
  * Set Standards-Version to 3.9.2 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 01 Oct 2011 16:54:19 +0200

libnet-twitter-perl (3.17001-1) unstable; urgency=low

  [ Alessandro Ghedini ]
  * New upstream release
  * Bump debhelper compat level to 8
  * Replace libtest-exception-per with libtest-fatal-perl in B-D-I
  * Add myself to Uploaders
  * Update upstream copyright years
  * Add (Build-)Depends(-Indep) on libmoosex-role-parameterized-perl
  * Add fix-bad-whatis-entry patch

  [ gregor herrmann ]
  * Add patch to use Digest::SHA (which is in perl core) instead of
    Digest::SHA1.

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 03 Apr 2011 19:58:01 +0200

libnet-twitter-perl (3.15000-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Sat, 26 Feb 2011 12:19:33 -0500

libnet-twitter-perl (3.14003-1) unstable; urgency=low

  * New upstream release
  * Refresh copyright information
  * Update (build-)deps per upstream

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 24 Feb 2011 21:15:02 -0500

libnet-twitter-perl (3.14002-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release (3.13009).
    + Now requires Devel::StackTrace per upstream
  * Add NEWS item detailing removal of basic authentication from
    Twitter upstream (only OAuth works; other services may still
    use basic authentication)

  [ Ansgar Burchardt ]
  * New upstream release (3.14002).
  * Add (build-)dep on libcrypt-ssleay-perl.
  * No longer run POD tests. Remove related build-dependencies and patch
    disable-pod-spelling.
  * Update my email address.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 14 Nov 2010 15:46:12 +0100

libnet-twitter-perl (3.13008-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: Refer to /usr/share/common-licenses/GPL-1; refer to
    "Debian systems" instead of "Debian GNU/Linux systems".
  * Bump Standards-Version to 3.9.1.

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 08 Sep 2010 20:38:06 +0900

libnet-twitter-perl (3.13007-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.0 (no changes).

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 07 Jul 2010 22:15:54 +0900

libnet-twitter-perl (3.13006-1) unstable; urgency=low

  * New upstream release.
  * Remove patch test_plan.patch, test count fixed upstream.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Jun 2010 15:58:25 +0200

libnet-twitter-perl (3.13004-1) unstable; urgency=low

  * New upstream release.
  * New patch test_plan.patch: use done_testing instead of a wrong plan for a
    test.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Jun 2010 01:23:08 +0200

libnet-twitter-perl (3.13003-1) unstable; urgency=low

  * New upstream release.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 22 May 2010 04:33:22 +0900

libnet-twitter-perl (3.13001-1) unstable; urgency=low

  * New upstream release

 -- Antony Gelberg <antony.gelberg@wayforth.com>  Wed, 12 May 2010 14:19:12 +0300

libnet-twitter-perl (3.13000-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 09 May 2010 17:52:15 -0400

libnet-twitter-perl (3.12000-1) unstable; urgency=low

  [ Franck Joncourt ]
  * New upstream release:
    + Removed no-network patch which prevented t/51_since.t from connecting to
      Twitter. t/51_since.t is now fixed.

 -- Franck Joncourt <franck@debian.org>  Fri, 23 Apr 2010 17:23:41 +0200

libnet-twitter-perl (3.11011-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release

  [ gregor herrmann ]
  * New build dependency: libtest-deep-perl.

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 12 Mar 2010 08:19:32 -0500

libnet-twitter-perl (3.11009-1) unstable; urgency=low

  * New upstream release
  * Update copyright information

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 10 Mar 2010 22:02:24 -0500

libnet-twitter-perl (3.11008-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 03 Mar 2010 10:30:31 -0500

libnet-twitter-perl (3.11007-1) unstable; urgency=low

  * New upstream release

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 28 Feb 2010 18:56:35 -0500

libnet-twitter-perl (3.11004-2) unstable; urgency=low

  * Updated patch no-network to require NETWORK_TEST in order to run
    t/51_since.t.
    Removed use of NETWORK_TEST in t/51_rate_limit.t.
    Fix FTBFS. (Closes: #571426)

 -- Franck Joncourt <franck@debian.org>  Thu, 25 Feb 2010 19:06:19 +0100

libnet-twitter-perl (3.11004-1) unstable; urgency=low

  [ Jonathan Yu ]
  * Now requires Moose 0.90
  * No longer requires MooseX::AttributeHelpers
  * Standards-Version 3.8.4 (no changes)

  [ Franck Joncourt ]
  * New upsrteam release
  * Added /me to Uploaders.
  * Updated the header of the disable-pod-spelling patch.
  * Switch to dpkg-source 3.0 (quilt) format:
    + Removed useless README.source which only documented the quilt usage.
    + Removed --with-quilt argument for debhelper.
    + Removed BDI on quilt in d.control.

 -- Franck Joncourt <franck@debian.org>  Wed, 10 Feb 2010 20:08:31 +0100

libnet-twitter-perl (3.10003-2) unstable; urgency=low

  * New patch no-network to avoid running network tests (closes: #564369).
  * Add /me to Uploaders and copyright.
  * Bump debhelper build dependency to ensure skipping Module::AutoInstall.
  * debian/rules: use an override for the test settings.
  * debian/watch: update to ignore development releases.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Jan 2010 21:33:21 +0100

libnet-twitter-perl (3.10003-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Taking over package (Closes: #544520)
  * Use new short debhelper rules file
  * Rewrite control description
  * Standards-Version 3.8.3 (drop perl version dep)

  [ Ryan Niebur ]
  * Add myself to Uploaders
  * enable some POD tests, patch to disable others

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 04 Jan 2010 22:27:24 -0500

libnet-twitter-perl (2.10-1) unstable; urgency=low

  * Initial release (Closes: #518181)

 -- Decklin Foster <decklin@red-bean.com>  Thu, 12 Mar 2009 12:11:19 -0400
